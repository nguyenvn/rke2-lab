curl -XPUT 'localhost:9200/*/_settings' \
  -H 'Content-Type: application/json' \
  -d '{ "index.number_of_replicas" : 0 }'

curl -XPUT -H 'Content-Type: application/json' 'localhost:9200/_cluster/settings' -d '{ "persistent" : {"cluster.max_shards_per_node" : 5000}}'

