
REF: https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner

$ helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/
$ helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner \
    --set nfs.server=x.x.x.x \
    --set nfs.path=/exported/path

# helm install rancher rancher-2.6.6.tgz 

helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner-4.0.16.tgz \
    --set nfs.server=10.252.59.43 \
    --set nfs.path=/nfsk8strading
