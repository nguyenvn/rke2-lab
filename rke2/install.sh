    #!/bin/bash

    # # Unpack
    # tar xzvf rke-government-deps-*.tar.gz

    INSTALL_TYPE=${1:-"server"}

    if [ "$INSTALL_TYPE" != "agent" ] && [ "$INSTALL_TYPE" != "server" ] ; then
      echo "Input INSTALL_TYPE must be either \"agent\" or \"server\""
      echo "Example: ./install.sh agent"
      exit 1
    fi

    if [ $# -eq 0 ]
      then
        echo "Defaulting to \"server\" type install. Pass in \"agent\" as parameter to install agent"
        echo "Example: ./install.sh agent"
    fi

    # Check if you can run Yum
    if command -v yum >/dev/null 2>&1; then

      # RHEL/Centos: Install using Yum
      # Make repo directory
      mkdir -p /var/lib/rancher/yum_repos

      # Unpack rpm repo into repo directory
      tar xzf rke_rpm_deps.tar.gz -C /var/lib/rancher/yum_repos

      # Create local rpm repo configuration
      echo "[rke_rpm_deps]" > /etc/yum.repos.d/rke_rpm_deps.repo
      echo "name=rke_rpm_deps" >> /etc/yum.repos.d/rke_rpm_deps.repo
      echo "baseurl=file:///var/lib/rancher/yum_repos/rke_rpm_deps" >> /etc/yum.repos.d/rke_rpm_deps.repo
      echo "enabled=0" >> /etc/yum.repos.d/rke_rpm_deps.repo
      echo "gpgcheck=0" >> /etc/yum.repos.d/rke_rpm_deps.repo

      # Install
      yum install -y --disablerepo=* --enablerepo="rke_rpm_deps" rke2-${INSTALL_TYPE}

    else

      # Ubuntu: Install using the binaries
      tar xzf ./rke2.linux-amd64.tar.gz
      /usr/bin/cp ./bin/rke2 /usr/local/bin/rke2
      /usr/bin/cp ./bin/rke2-uninstall.sh /usr/local/bin/rke2-uninstall.sh
      /usr/bin/cp ./bin/rke2-killall.sh /usr/local/bin/rke2-killall.sh
      /usr/bin/cp ./lib/systemd/system/rke2-${INSTALL_TYPE}.env /etc/systemd/system/rke2-${INSTALL_TYPE}.env
      /usr/bin/cp ./lib/systemd/system/rke2-${INSTALL_TYPE}.service /etc/systemd/system/rke2-${INSTALL_TYPE}.service
      /usr/bin/mkdir -p /usr/share/rke2
      /usr/bin/cp ./share/rke2/rke2-cis-sysctl.conf /usr/share/rke2/rke2-cis-sysctl.conf
      systemctl daemon-reload

    fi

    # Make images directory
    mkdir -p /var/lib/rancher/rke2/agent/images/

    # Unpack and copy images into images directory
    cp rke2-images-core.linux-amd64.tar.zst /var/lib/rancher/rke2/agent/images/
    cp rke2-images-canal.linux-amd64.tar.zst /var/lib/rancher/rke2/agent/images/

    echo "Done"
    exit 0

    # # If you're using a CIS profile setting, you need to perform additional steps (https://docs.rke2.io/security/hardening_guide/)
    # cp -f /usr/share/rke2/rke2-cis-sysctl.conf /etc/sysctl.d/60-rke2-cis.conf
    # systemctl restart systemd-sysctl
    # useradd -r -c "etcd user" -s /sbin/nologin -M etcd

    # Now add RKE2 configuration at /etc/rancher/rke2/config.d/, and start the service with 'systemctl start rke2-server' or 'systemctl start rke2-agent'
    # Configuration reference https://docs.rke2.io/install/install_options/server_config/
    # Configuration reference https://docs.rke2.io/install/install_options/agent_config/
