#!/bin/bash

# Download deps for centos 8
# docker run -it --rm \
# --mount type=bind,source="$(pwd)",target=/mnt -w /mnt \
# centos:8 \
# /bin/bash ./offline_rke2_tutorial_dep_collection.sh

# Download deps for centos 7
# docker run -it --rm \
# --mount type=bind,source="$(pwd)",target=/mnt -w /mnt \
# centos:7 \
# /bin/bash ./offline_rke2_tutorial_dep_collection.sh

# Pass in SKIP_IMAGES_DL to skip downloading images tar file
# -e SKIP_IMAGES_DL="true" \

set -e

SKIP_IMAGES_DL=${SKIP_IMAGES_DL:-'false'}

setup_env() {

  INSTALL_RKE2_CHANNEL="stable"
  INSTALL_RKE2_METHOD="yum"
  # --- bail if we are not root ---
  if [ ! $(id -u) -eq 0 ]; then
      fatal "You need to be root to perform this install"
  fi

  # --- make sure install type has a value
  if [ -z "${INSTALL_RKE2_TYPE}" ]; then
      INSTALL_RKE2_TYPE="server"
  fi

  if [ -z "${INSTALL_RKE2_AGENT_IMAGES_DIR}" ]; then
      INSTALL_RKE2_AGENT_IMAGES_DIR="/var/lib/rancher/rke2/agent/images"
  fi
}

setup_arch() {
  case ${ARCH:=$(uname -m)} in
  amd64)
      ARCH=amd64
      SUFFIX=$(uname -s | tr '[:upper:]' '[:lower:]')-${ARCH}
      ;;
  x86_64)
      ARCH=amd64
      SUFFIX=$(uname -s | tr '[:upper:]' '[:lower:]')-${ARCH}
      ;;
  *)
      fatal "unsupported architecture ${ARCH}"
      ;;
  esac
}

get_release_version() {
  if [ -n "${INSTALL_RKE2_COMMIT}" ]; then
      version="commit ${INSTALL_RKE2_COMMIT}"
  elif [ -n "${INSTALL_RKE2_VERSION}" ]; then
      version=${INSTALL_RKE2_VERSION}
  else
      echo "finding release for channel ${INSTALL_RKE2_CHANNEL}"
      INSTALL_RKE2_CHANNEL_URL=${INSTALL_RKE2_CHANNEL_URL:-'https://update.rke2.io/v1-release/channels'}
      version_url="${INSTALL_RKE2_CHANNEL_URL}/${INSTALL_RKE2_CHANNEL}"
      version=$(curl -w "%{url_effective}" -L -s -S ${version_url} -o /dev/null | sed -e 's|.*/||')
      INSTALL_RKE2_VERSION="${version}"
  fi
}

install_conf() {
  maj_ver="7"
  if [ -r /etc/redhat-release ] || [ -r /etc/centos-release ] || [ -r /etc/oracle-release ]; then
      dist_version="$(. /etc/os-release && echo "$VERSION_ID")"
      maj_ver=$(echo "$dist_version" | sed -E -e "s/^([0-9]+)\.?[0-9]*$/\1/")
      case ${maj_ver} in
          7|8)
              :
              ;;
          *) # In certain cases, like installing on Fedora, maj_ver will end up being something that is not 7 or 8
              maj_ver="7"
              ;;
      esac
  fi
  case "${INSTALL_RKE2_CHANNEL}" in
      v*.*)
          # We are operating with a version-based channel, so we should parse our version out
          rke2_majmin=$(echo "${INSTALL_RKE2_CHANNEL}" | sed -E -e "s/^v([0-9]+\.[0-9]+).*/\1/")
          rke2_rpm_channel=$(echo "${INSTALL_RKE2_CHANNEL}" | sed -E -e "s/^v[0-9]+\.[0-9]+-(.*)/\1/")
          # If our regex fails to capture a "sane" channel out of the specified channel, fall back to `stable`
          if [ "${rke2_rpm_channel}" = ${INSTALL_RKE2_CHANNEL} ]; then
              info "using stable RPM repositories"
              rke2_rpm_channel="stable"
          fi
          ;;
      *)
          get_release_version
          rke2_majmin=$(echo "${INSTALL_RKE2_VERSION}" | sed -E -e "s/^v([0-9]+\.[0-9]+).*/\1/")
          rke2_rpm_channel=${1}
          ;;
  esac
  echo "using ${rke2_majmin} series from channel ${rke2_rpm_channel}"
  rpm_site="rpm.rancher.io"
  if [ "${rke2_rpm_channel}" = "testing" ]; then
      rpm_site="rpm-${rke2_rpm_channel}.rancher.io"
  fi
  rm -f /etc/yum.repos.d/rancher-rke2*.repo
  cat <<-EOF >"/etc/yum.repos.d/rancher-rke2.repo"
[rancher-rke2-common-${rke2_rpm_channel}]
name=Rancher RKE2 Common (${1})
baseurl=https://${rpm_site}/rke2/${rke2_rpm_channel}/common/centos/${maj_ver}/noarch
enabled=1
gpgcheck=1
gpgkey=https://${rpm_site}/public.key
[rancher-rke2-${rke2_majmin}-${rke2_rpm_channel}]
name=Rancher RKE2 ${rke2_majmin} (${1})
baseurl=https://${rpm_site}/rke2/${rke2_rpm_channel}/${rke2_majmin}/centos/${maj_ver}/x86_64
enabled=1
gpgcheck=1
gpgkey=https://${rpm_site}/public.key
EOF
  rke2_rpm_version=$(echo "${INSTALL_RKE2_VERSION}" | sed -E -e "s/[\+-]/~/g" | sed -E -e "s/v(.*)/\1/")
}

fatal() {
  echo "$!"
  exit
}

do_download() {

  if [ "${SKIP_IMAGES_DL}" = "false" ]; then
    curl -LO ${RKE_IMAGES_DL_SHASUM}

    if [ "${OLD_ASSETS}" == 1 ]; then
      # OLD SCHEMA: rke2-images.
      # grab and verify rke2.images
      curl -LO ${RKE_CORE_IMAGES_DL_URL};
      CHECKSUM_ACTUAL=$(sha256sum "rke2-images.linux-amd64.tar.zst" | awk '{print $1}');
      CHECKSUM_EXPECTED=$(grep "rke2-images.linux-amd64.tar.zst" "sha256sum-amd64.txt" | awk '{print $1}');
      if [ "${CHECKSUM_EXPECTED}" != "${CHECKSUM_ACTUAL}" ]; then echo "FATAL: RKE_CORE_IMAGES_DL_URL download sha256 does not match"; exit 1; fi

    else
      # NEW SCHEMA: rke2-images-core, rke2-images-canal
      # grab and verify rke2-images-core
      curl -LO ${RKE_CORE_IMAGES_DL_URL};
      CHECKSUM_ACTUAL=$(sha256sum "rke2-images-core.linux-amd64.tar.zst" | awk '{print $1}');
      CHECKSUM_EXPECTED=$(grep "rke2-images-core.linux-amd64.tar.zst" "sha256sum-amd64.txt" | awk '{print $1}');
      if [ "${CHECKSUM_EXPECTED}" != "${CHECKSUM_ACTUAL}" ]; then echo "FATAL: RKE_CORE_IMAGES_DL_URL download sha256 does not match"; exit 1; fi
      # grab and verify rke2-images-canal
      curl -LO ${RKE_CANAL_IMAGES_DL_URL};
      CHECKSUM_ACTUAL=$(sha256sum "rke2-images-canal.linux-amd64.tar.zst" | awk '{print $1}');
      CHECKSUM_EXPECTED=$(grep "rke2-images-canal.linux-amd64.tar.zst" "sha256sum-amd64.txt" | awk '{print $1}');
      if [ "${CHECKSUM_EXPECTED}" != "${CHECKSUM_ACTUAL}" ]; then echo "FATAL: RKE_CANAL_IMAGES_DL_URL download sha256 does not match"; exit 1; fi
    fi


    # Grab binaries
    curl -LO ${RKE_BINARIES_DL_URL};
    rm -f sha256sum-amd64.txt
  fi

  # download all rpms and their dependencies
  case ${maj_ver} in
    7)
      mkdir rke_rpm_deps;
      cd rke_rpm_deps;
      yum install -y --releasever=/ --installroot=$(pwd) --downloadonly --downloaddir $(pwd) ${YUM_PACKAGES};
      createrepo -v .;
      cd ..;
      tar -cvf rke_rpm_deps.tar rke_rpm_deps;
      rm -rf rke_rpm_deps;
      pigz rke_rpm_deps.tar
      ;;
    8)
      dnf install -y epel-release
      ## download from epel when packages are fixed
      curl -LO http://mirror.centos.org/centos/8-stream/AppStream/x86_64/os/Packages/modulemd-tools-0.7-4.el8.noarch.rpm
      dnf install -y ./modulemd-tools-0.7-4.el8.noarch.rpm;
      rm -f modulemd-tools-0.7-4.el8.noarch.rpm

      mkdir -p rke_rpm_deps/Packages;
      cd rke_rpm_deps/Packages;
      yum install -y --releasever=/ --installroot=$(pwd) --downloadonly --downloaddir $(pwd) ${YUM_PACKAGES};
      cd ..
      createrepo_c .;
      repo2module  -s stable -d . modules.yaml;
      modifyrepo_c --mdtype=modules ./modules.yaml ./repodata;
      cd ..;
      tar -cvf rke_rpm_deps.tar rke_rpm_deps;
      rm -rf rke_rpm_deps;
      pigz rke_rpm_deps.tar
      ;;
    *)
      :
      ;;
  esac
}

setup_env
setup_arch
install_conf "stable"

RKE2_VERSION_SUFFIX=${rke2_rpm_version#*\~}
RKE2_VERSION_FULL=${rke2_rpm_version%\~*}
YUM_PACKAGES="unzip rke2-server-$rke2_rpm_version rke2-agent-$rke2_rpm_version"

# test to see what asset schema we are on. Newer versions split everything out. Older versions only use rke2-images.
echo "$(curl -sIL -w "%{http_code}" "https://github.com/rancher/rke2/releases/download/v${RKE2_VERSION_FULL}%2B${RKE2_VERSION_SUFFIX}/rke2-images-canal.linux-amd64.tar.zst") != 200"

if [[ $(curl -sIL -w "%{http_code}" "https://github.com/rancher/rke2/releases/download/v${RKE2_VERSION_FULL}%2B${RKE2_VERSION_SUFFIX}/rke2-images-canal.linux-amd64.tar.zst") != 200 ]]; then
  OLD_ASSETS=1
else
  OLD_ASSETS=0
fi
