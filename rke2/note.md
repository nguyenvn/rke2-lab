
cluster-cidr: "10.42.0.0/16"
service-cidr: "10.43.0.0/16"
kubelet-arg:
  - "eviction-hard=imagefs.available<10%,memory.available<100Mi,nodefs.available<10%,nodefs.inodesFree<5%"
  - "eviction-pressure-transition-period=5m0s"
  - "image-gc-high-threshold=65"
  - "image-gc-low-threshold=50"
  - "oom-score-adj=-999"
  - "v=8"
cni:
  - canal
disable-cloud-controller: true
debug: true


data-dir: "/opt/rancher/rke2"
disable: rke2-ingress-nginx


kubelet-arg:
- "root-dir=/opt/rancher/kubelet" 
- "max-pods=500" 


mount | grep io.containerd | grep '/run/k3s/' | awk '{ print $3 }' | xargs umount -l
mount | grep tmpfs | grep '/var/lib/kubelet' | awk '{ print $3 }' | xargs umount -l

ip addr del XXXXX/32 dev ens192


########################


sudo mkdir /data/k3s
sudo ln -s /data/k3s /run/k3s
sudo mkdir -p /data/rancher/rke2
sudo mkdir -p /data/rancher/kubelet
mkdir -p /data/rancher/rke2/agent/images/
cp /opt/ok/RKE_Dependencies/22/rke2-images-core.linux-amd64.tar.zst /data/rancher/rke2/agent/images/
cp /opt/ok/RKE_Dependencies/22/rke2-images-canal.linux-amd64.tar.zst /data/rancher/rke2/agent/images/


cluster-cidr: "10.42.0.0/16"
service-cidr: "10.43.0.0/16"
kubelet-arg:
  - "eviction-hard=imagefs.available<10%,memory.available<100Mi,nodefs.available<10%,nodefs.inodesFree<5%"
  - "eviction-pressure-transition-period=5m0s"
  - "image-gc-high-threshold=65"
  - "image-gc-low-threshold=50"
  - "oom-score-adj=-999"
  - "v=8"
  - "root-dir=/data/rancher/kubelet" 
  - "max-pods=180" 
data-dir: "/data/rancher/rke2"



############

#!/bin/sh
docker rm -f $(docker ps -qa)
docker volume rm $(docker volume ls -q)
cleanupdirs="/var/lib/etcd /etc/kubernetes /etc/cni /opt/cni /var/lib/cni /var/run/calico /opt/rke"
for dir in $cleanupdirs; do
echo "Removing $dir"
rm -rf $dir
done

docker rm -f $(docker ps -qa) docker rmi -f $(docker images -q) docker volume rm $(docker volume ls -q)

for mount in $(mount | grep tmpfs | grep '/var/lib/kubelet' | awk '{ print $3 }') /var/lib/kubelet /var/lib/rancher; do umount $mount; done

rm -rf /etc/ceph \ /etc/cni \ /etc/kubernetes \ /opt/cni \ /opt/rke \ /run/secrets/kubernetes.io \ /run/calico \ /run/flannel \ /var/lib/calico \ /var/lib/etcd \ /var/lib/cni \ /var/lib/kubelet \ /var/lib/rancher/rke/log \ /var/log/containers \ /var/log/kube-audit \ /var/log/pods \ /var/run/calico

