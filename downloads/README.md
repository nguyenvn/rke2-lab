# Retrieve kubeconfig from RKE v0.1.x or Rancher v2.0.x/v2.1.x custom cluster controlplane node 

For RKE v0.2.x and Rancher v2.2.x, see https://gist.github.com/superseb/b14ed3b5535f621ad3d2aa6a4cd6443b

Applicable for:
* RKE v0.1.x 
* Rancher v2.0.x
* Rancher v2.1.x

## Oneliner (RKE and Rancher custom cluster)

If you know what you are doing (requires `kubectl` and `jq` on the node).

```
kubectl --kubeconfig $(docker inspect kubelet --format '{{ range .Mounts }}{{ if eq .Destination "/etc/kubernetes" }}{{ .Source }}{{ end }}{{ end }}')/ssl/kubecfg-kube-node.yaml get secret -n kube-system kube-admin -o jsonpath={.data.Config} | base64 -d | sed -e "/^[[:space:]]*server:/ s_:.*_: \"https://127.0.0.1:6443\"_" > kubeconfig_admin.yaml
```

```
kubectl --kubeconfig kubeconfig_admin.yaml get nodes
```

## Docker run commands (Rancher custom cluster)

To be executed on nodes with `controlplane` role, this uses the `rancher/rancher-agent` image to retrieve the kubeconfig.

1. Get kubeconfig

```
docker run --rm --net=host -v $(docker inspect kubelet --format '{{ range .Mounts }}{{ if eq .Destination "/etc/kubernetes" }}{{ .Source }}{{ end }}{{ end }}')/ssl:/etc/kubernetes/ssl:ro --entrypoint bash $(docker inspect $(docker images -q --filter=label=io.cattle.agent=true) --format='{{index .RepoTags 0}}' | tail -1) -c 'kubectl --kubeconfig /etc/kubernetes/ssl/kubecfg-kube-node.yaml get secret -n kube-system kube-admin -o jsonpath={.data.Config} | base64 -d | sed -e "/^[[:space:]]*server:/ s_:.*_: \"https://127.0.0.1:6443\"_"' > kubeconfig_admin.yaml
```

2. Run `kubectl get nodes`

```
docker run --rm --net=host -v $PWD/kubeconfig_admin.yaml:/root/.kube/config --entrypoint bash $(docker inspect $(docker images -q --filter=label=io.cattle.agent=true) --format='{{index .RepoTags 0}}' | tail -1) -c 'kubectl get nodes'
```

## Script
Run `rke-node-kubeconfig.sh` and follow instructions given